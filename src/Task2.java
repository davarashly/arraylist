/*

Задача 2. Создать arraylist типа string и заполнить его 10 строками с клавиатуры. С
помощью цикла найти строку максимально длины(или несколько, если таковые
имеются) и вывести её(их) на экран.

 */

import swag.features.TextColors;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Task2 {
    public static void main(String[] args) throws IOException {
        ArrayList<String> strings = new ArrayList<>();
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

        for (int j = 0; j < 10; j++)
            strings.add(in.readLine());

        int max = strings.get(0).length();

        for (String string : strings)
            if (max < string.length())
                max = string.length();

        for (String string : strings)
            if (string.length() == max)
                System.out.println(string);
    }
}
