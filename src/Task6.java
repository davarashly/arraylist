/*

Задача 6. Создать arraylist типа string и заполнить его 10 строками
с клавиатуры. С помощью цикла найти самую маленькую или
самую длинную строку, в зависимости от того, какая встретится
первой

 */

import java.util.ArrayList;

public class Task6 {
    public static void main(String[] args) {
        ArrayList<String> strings = new ArrayList<>();

        strings.add("GJGJGJGD");
        strings.add("HNL;LKD;G");
        strings.add("ASDA");

        int max = Integer.MIN_VALUE, min = Integer.MAX_VALUE;

        for (String string : strings) {
            if (max < string.length())
                max = string.length();
            if (min > string.length())
                min = string.length();
        }

        for (String string : strings) {
            if (string.length() == min) {
                System.out.println(string);
                break;
            }
            if (string.length() == max) {
                System.out.println(string);
                break;
            }
        }

    }
}
