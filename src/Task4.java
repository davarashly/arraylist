/*

Задача 4. Создать arraylist типа string произвольной длины. Добавлять в него строки.
Но не в конец, а в начало списка.

 */

import swag.features.TextColors;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Task4 {
    public static void main(String[] args) throws IOException {
        ArrayList<String> strings = new ArrayList<>();
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

        for (int j = 0; j < 3; j++)
            strings.add(0, in.readLine());

        for (String string : strings)
            System.out.println(string);


    }
}
