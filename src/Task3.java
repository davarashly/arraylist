/*

Задача 3.Создать arraylist типа string и заполнить его 10 строками с клавиатуры. С
помощью цикла найти строку минимальной длины(или несколько, если таковые
имеются) и вывести её(их) на экран.

 */

import swag.features.TextColors;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Task3 {
    public static void main(String[] args) throws IOException {
        ArrayList<String> strings = new ArrayList<>();
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

        for (int j = 0; j < 3; j++)
            strings.add(in.readLine());

        int min = strings.get(0).length();

        for (String string : strings)
            if (min > string.length())
                min = string.length();

        for (String string : strings)
            if (string.length() == min)
                System.out.println(string);
    }
}
