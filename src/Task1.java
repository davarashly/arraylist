/*

Задача 1. Создать arraylist типа string и заполнить его 10 строками. Вывести на экран
содержимое списка.

 */

import swag.features.TextColors;

import java.util.ArrayList;

public class Task1 {
    public static void main(String[] args) {
        ArrayList<String> strings = new ArrayList<>();

        for (int j = 0; j < 10; j++)
            strings.add("string " + (j + 1));

        for (String string : strings)
            System.out.println(string);

    }
}
