/*

Задача 5. Создать arraylist типа string и заполнить его 10 строками. Произвести
циклический сдвиг вправо 17 раз и вывести на экран.

 */

import swag.features.TextColors;

import java.util.ArrayList;


public class Task5 {
    public static void main(String[] args) {
        ArrayList<String> strings = new ArrayList<>();

        int a = 4;

        strings.add("LOL");
        strings.add("KEK");
        strings.add("CHEBUREK");

        for (int i = 0; i < a; i++) {
            strings.add(0, strings.get(strings.size() - 1));
            strings.remove(strings.size() - 1);
        }

        for (String string : strings)
            System.out.println(string);
    }
}
